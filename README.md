# upload-files-django-rest

## Version01


    @transaction.atomic()
    @action(methods=['POST'], detail=False, url_path='files/xxxx', serializer_class=XXXXFilesSerializer)
    def create_files(self, request, *args, **kwargs):
        
        print('== request.data == ',request.data)
        print('== request.data[file] == ',request.data['file'])
        print('== request.FILES == ',request.FILES)

        data = request.FILES
        print('== data == ', data)
        uploadedFiles = data.getlist('file')
        print('== uploadedFiles == ', uploadedFiles)
        for single_file in uploadedFiles :
            print('== single_file == ',single_file)
            new_data = data
            print('== new_data before== ', new_data)
            new_data['xxx'] = ####
            new_data['file'] = single_file
            print('== new_data after == ', new_data)
            serializer = self.serializer_class(data=new_data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        return Response('save')
        return Response(serializer.data)

## Version02

    @transaction.atomic()
    @action(methods=['POST'], detail=False, url_path='files/xxxx', serializer_class=XXXXFilesSerializer)
    def create_files(self, request, *args, **kwargs):
        
        print('== request.data == ',request.data)
        print('== request.data[file] == ',request.data['file'])
        print('== request.FILES == ',request.FILES)

        data = request.FILES
        print('== data == ', data)
        uploadedFiles = data.getlist('file')
        print('== uploadedFiles == ', uploadedFiles)
        for single_file in uploadedFiles :

            new_data = {}
            new_data['xxx'] = ####
            new_data['file'] = single_file

            print('== new_data == ', new_data)

            serializer = self.serializer_class(data=new_data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        return Response('save')
        return Response(serializer.data)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
